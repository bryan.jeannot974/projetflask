from flask import Flask
app = Flask(__name__)
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager



Bootstrap(app)
app.config['BOOTSTRAP_SERVE_LOCAL'] = True

import os.path
def mkpath(p):
        """
         renvoie repertoire courant
        """
        return os.path.normpath(
                os.path.join(
                        os.path.dirname(__file__),
                        p))



app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'
+mkpath('../books.db'))
app.config['SECRET_KEY'] = "1f133d3c-998f-4e70-8313-905d5169d5fe"

db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)

from .models import User

@login_manager.user_loader
def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))
