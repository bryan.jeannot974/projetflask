from tkinter import N
from .app import db
from flask_login import UserMixin
from sqlalchemy import exc

"""
A ajouter en fonction du MCD 

"""

class Author(db.Model):
    """
    Classe Auteur
    """
    pokedex_number = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    def __repr__(self):
        return "<Author (%d) %s>" % (self.id, self.name)


    

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))

    #pokemons = db.relationship(lazy='subquery',backref=db.backref('musics', lazy=True)

class Book(db.Model):
    """
    Identiter pokemon 
    """
    pokedex_number = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    generation = db.Column(db.Integer)
    classfication = db.Column(db.Integer)
    type1 = db.Column(db.String(120))
    type2 = db.Column(db.String(120))       

    hp = db.Column(db.Integer)
    height_m = db.Column(db.Float)
    weight_kg = db.Column(db.Float)
    speed = db.Column(db.Integer)
    percentage_male = db.Column(db.Float)
    is_legendary = db.Column(db.Integer)


    def __repr__(self):
      return "<Pokemon (%d) %s>" % (self.pokedex_number, self.name)

class asso(db.Model ):
    __table_args__ = (db.PrimaryKeyConstraint('idEquipe', 'idPokemon') , )
    
    idEquipe =db.Column(db.Integer, db.ForeignKey("equipe.idEquipe"))
    idPokemon = db.Column(db.Integer, db.ForeignKey("book.pokedex_number"))


class Equipe(db.Model):
    """
    Id 
    """
    idEquipe=db.Column(db.Integer, primary_key=True)
    idUser =db.Column(db.Integer, db.ForeignKey("user.id"))
    TitreEquipe = db.Column(db.String(120))
    pokemons = db.relationship("Book", secondary="asso" ,backref=db.backref("equipe", lazy="dynamic"))
    user = db.relationship("User" ,backref=db.backref("equipes", lazy="dynamic"))




def CheckEquipe(): 
    return Equipe.query.limit(100).all()


def rechercheAsso(idEquipe) :
    return asso.query.filter(asso.idEquipe==idEquipe )

def creeEquipe(id , nom): 
    val = Equipe(
        idUser = id ,
        TitreEquipe = nom
    )
    db.session.add(val)
    #db.session.add(EquipeS)
    return db.session.commit()

def ListeCompte(idCompte) :
    return User.query.filter(User.id == idCompte )

def affichageNumEquipeCree(idEquipe) : 
    return Equipe.query.filter(asso.idEquipe==idEquipe )

def getTitreNameById(id): 
    return Equipe.query.filter(Equipe.idEquipe==id )

def CreationEquipe(id , pokemon): 

    try:
        ajouterVal = asso(
        idEquipe =id,
        idPokemon = pokemon)
        db.session.add(ajouterVal)
        return db.session.commit()
     
    except exc.IntegrityError:
     db.session.rollback()
     return "false"

def suppressionPokemonParEquipe(idEquipe ,idPoke ):
    
    obj = asso.query.filter(asso.idEquipe==idEquipe ,asso.idPokemon == idPoke  ).one()
    db.session.delete(obj)
    return db.session.commit()




def get_sample():
  return Book.query.all()


def SupressionCompte(idCompte): 
    obj = User.query.filter(User.id == idCompte  ).one()
    db.session.delete(obj)
    return db.session.commit()

def getEquipeParId(id) : 
    return Equipe.query.filter(Equipe.idUser==id )


def getPokemonParNom(nomPoke): 
    return Book.query.filter(Book.name==nomPoke )

def getPokemonById(ID): 
    return Book.query.filter(Book.pokedex_number==ID )

def majUser(id ,email2 ,nomUtilisateur): 
    User.query.filter(User.id==id).update(dict( email = email2,name = nomUtilisateur ))
    return db.session.commit()
 

    
    
def getPokemonIn(): 
    return Equipe.query.limit(100).all()


def AjouterPokemon(NomPokemon , Generation , Classification , type1P , type2P , HP ,height_mP ,weight_kgP ,speedP ,percentage_maleP,is_legendaryP  ): 
    print ("ddzdzd")
    Pokemon  = Book(name   = NomPokemon,
        generation   = Generation,
        classfication     = Classification,
        type1   = type1P,
        type2   = type2P,
        hp   = HP,
        height_m   = height_mP,
        weight_kg   = weight_kgP,
        speed   = speedP,
        percentage_male   = percentage_maleP,
        is_legendary   = is_legendaryP) 
        
    db.session.add(Pokemon)
    return db.session.commit()

def editions( id,NomPokemon , HP ,height_mP ,weight_kgP ,speedP ,percentage_maleP): 
    print ("function")
    Book.query.filter(Book.pokedex_number==id).update(dict(name=NomPokemon , hp =HP , height_m = height_mP ,weight_kg = weight_kgP , speed = speedP  ,percentage_male = percentage_maleP))

    return db.session.commit()

def deletePokemon(idPoke): 
    obj = Book.query.filter(Book.pokedex_number == idPoke  ).one()
    db.session.delete(obj)
    return db.session.commit()

def FiltreType1(type):
    return Book.query.filter(Book.type1==type)


#la
def Trieother(type ,type2s, generationI , legend):
    return Book.query.filter( Book.generation == generationI ,  Book.type1 == type ,Book.type2==type2s, Book.is_legendary==legend ) 

def TrieotherAll(type ,type2s, legend):
    return Book.query.filter(   Book.type1 == type ,Book.type2==type2s, Book.is_legendary==legend ) 

def FiltreType2(type2): 
    """
    Filtre de type2 
    """
    return Book.query.filter(Book.type2==type2)

def rechercheID(id): 
    return Book.query.filter(Book.pokedex_number==id) 

def recherche(name): 
    return Book.query.filter(Book.name==name) 

def FiltreType(type , type2):
    """
    Filtre des deux type 
    """
    return Book.query.filter(Book.type1==type and Book.type2==type2)


def get_author(id):
	return Author.query.get_or_404(id)

def exist_author(name):
    return Author.query.filter(Author.name==name).one()
    
def get_book_detail(bookid):
    return Book.query.get_or_404(bookid)

