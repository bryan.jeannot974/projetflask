from wtforms.fields.simple import PasswordField
from .app import app, db
from flask import render_template, redirect, url_for , request , flash
from .models import Author, Equipe,User, get_sample,SupressionCompte, get_book_detail,majUser, ListeCompte, get_author ,deletePokemon,CreationEquipe,rechercheID, FiltreType , FiltreType1 , recherche ,AjouterPokemon ,editions,Trieother,TrieotherAll ,CreationEquipe,getPokemonIn,getEquipeParId, getPokemonParNom , rechercheAsso ,getPokemonById, creeEquipe , CheckEquipe,suppressionPokemonParEquipe
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, validators , PasswordField , SelectField ,IntegerField
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_required, current_user
from flask_login import  logout_user
from flask_login import login_user

class RegistrationForm(FlaskForm):
        NomPokemon = StringField('NomPokemon')
       


class Logins(FlaskForm):
        autre = StringField('autre')
        password = PasswordField('pass')

class trie(FlaskForm): 
    generation  = SelectField('liste Type', choices=[(1,"1"),(2,"2"),(3,"3"),(4,"4"),(5,"5"),(6,"6"),(7,"7") , (8,"all")])
    typeTrie  = SelectField('liste Type', choices=[("fire","fire"),("flying","flying"),("poison","poison"),("normal","normal"),("fight","fight"),("poison","poison"),("ground","ground"),("rock","rock"),("bug","bug"),("ghost","ghost"),("steel","steel"),("water","water"),("grass","grass"),("electric","electric"),("psychic","psychic"),("ice","ice"),("dragon","dragon"),("dark","dark"),("fairy","fairy")])   
    typeTrie2  = SelectField('liste Type', choices=[("none","Aucun type"),("flying","flying"),("fire","fire"),("poison","poison"),("normal","normal"),("fight","fight"),("poison","poison"),("ground","ground"),("rock","rock"),("bug","bug"),("ghost","ghost"),("steel","steel"),("water","water"),("grass","grass"),("electric","electric"),("psychic","psychic"),("ice","ice"),("dragon","dragon"),("dark","dark"),("fairy","fairy")])   

    is_legendary = SelectField('liste Type', choices=[(1,"Legendaire"),(2,"No_Legendaire")])
    MinW = IntegerField('2')
    MaxH = IntegerField('3')
    MinH = IntegerField('4')



class ajouters(FlaskForm): 
        NomPokemon = StringField('NomPokemon')
        generation = IntegerField('Generateurs')
        Classification  = IntegerField('Classification')

        type12  = SelectField('liste Type', choices=[("fire","fire"),("poison","poison"),("normal","normal"),("fight","fight"),("poison","poison"),("ground","ground"),("rock","rock"),("bug","bug"),("ghost","ghost"),("steel","steel"),("water","water"),("grass","grass"),("electric","electric"),("psychic","psychic"),("ice","ice"),("dragon","dragon"),("dark","dark"),("fairy","fairy")])   
        type21  = SelectField('liste Type', choices=[("fire","fire"),("poison","poison"),("normal","normal"),("fight","fight"),("poison","poison"),("ground","ground"),("rock","rock"),("bug","bug"),("ghost","ghost"),("steel","steel"),("water","water"),("grass","grass"),("electric","electric"),("psychic","psychic"),("ice","ice"),("dragon","dragon"),("dark","dark"),("fairy","fairy")])   

        HP = IntegerField('HP')
        height_m = IntegerField('height_m')
        weight_kg = IntegerField('weight_kg')

        speed = IntegerField('speed')
        percentage_male = IntegerField('percentage_male')
        is_legendary = SelectField('liste Type', choices=[(1,"Legendaire"),(2,"No_Legendaire")])



class GForm(FlaskForm):
    username = StringField('ba')


class recherTeam(FlaskForm): 
    teams = IntegerField('b') 

class ajoutPpkemon(FlaskForm) : 
    is_legendary = SelectField('liste Type')


@app.route("/", methods=("GET", "POST"))
def home():
    f = GForm()
    if f.validate_on_submit():
        return redirect(url_for('success', id=f.username.data))
    return render_template("base.html",form = f   )

@app.route('/profile' , methods=("GET", "POST"))
@login_required
def profile():
    f = GForm()
    fs = recherTeam()

    dict1 = dict()

    listePoke = []
    listeEquipe = []
    val = getEquipeParId(current_user.id) 
    for x in val : 
        listeEquipe.append(x)
        listePoke = []
        for y in rechercheAsso(x.idEquipe):
            for z in getPokemonById(y.idPokemon) : 
                listePoke.append(z)  
        dict1[x.idEquipe] =  listePoke 
    
    

 
    if request.method == 'POST' and  fs.teams.data != 'None':
        print("ici")
        return redirect(url_for('rechercheParEquipe', id=int(fs.teams.data)))


    return render_template('profile.html', name=current_user.name,form = f , other = fs , pokemon = AfficherLesPokemonEquipe(1) , listeEquip =listeEquipe , dict12 = dict1 )




@app.route('/rechercheParEquipe/<int:id>', methods=("GET", "POST"))
@login_required
def rechercheParEquipe(id):
    f = GForm()
    fs = recherTeam()
    if request.method == 'POST' : 
        suppressionPokemonParEquipe(int(id) ,int(request.form.get('IDpoke')) )
    return render_template('rechercheParEquipe.html', name=current_user.name,form = f  ,forms = fs, pokemon = AfficherLesPokemonEquipe(int(id) ) , idval = int(id) )

def AfficherLesPokemonEquipe(id): 
    val = getEquipeParId(current_user.id) 
    listePoke = []
    for y in rechercheAsso(id):
            for z in getPokemonById(y.idPokemon) : 
                listePoke.append(z)  
    return listePoke

@app.route('/creationEquipe/' , methods=("GET", "POST"))
@login_required
def creationEquipe():
    f = GForm()
    fs = recherTeam()
    if request.method == 'POST' : 
        creeEquipe(current_user.id , str (request.form.get('NomEquipe')))

    return render_template('creationEquipe.html', name=current_user.name,form = f , other = fs , affiche = CheckEquipe()  )




@app.route('/AjoutPokemonEquipe/'  , methods=("GET", "POST"))
@login_required
def AjoutPokemonEquipe():
    #CreationEquipe(2)
    f = GForm()
    fs = recherTeam()
    val = getEquipeParId(current_user.id)
    val2 = get_sample() 
    val3 = ""

    if request.method == 'POST' : 
        equipe = request.form.get('equipe')
        pokemon = request.form.get('poke')
        print(pokemon)
        print(equipe)
        result = CreationEquipe(int(equipe) , int(pokemon))
        if (result == "false"): 
            val3 = "Le pokemon que vous voulez ajouter existe deja dans cette equipe "
        else :
            val3 = "pokemon bien ajouter " 

    return render_template('ajoutPokemonDansEquipe.html', name=current_user.name,form = f , other = fs  , listeEquipe = val , listePoke = val2 , erreur = val3 )




@app.route('/modification/<int:id>', methods=("GET", "POST") )
def edition(id):
    f = GForm()
    print(int(id))
    if request.method == 'POST' : 
        print ("la")
        name = request.form.get('name')
        hp = request.form.get('hp')
        taille = request.form.get('height_m')
        poid =request.form.get('weight_kg')
        vitesse = request.form.get('speed')
        porcentageMale = request.form.get('percentage_male')
        if (porcentageMale != None): 
            editions(int(id) , str(name) , int(hp) , float(taille) , float(poid) , int(vitesse)  , float(porcentageMale) )
    return render_template('edition.html' , books=rechercheID(int(id)), form = f  )


@app.route('/supression/<int:id>', methods=("GET", "POST") )
def supr(id):
    f = GForm()
    print(int(id))
    if request.method == 'POST' : 
        deletePokemon(int(id))  
        return redirect(url_for('Accueil')) 
    return render_template('homes.html' , form = f  )



@app.route('/signup')
def signup():
    f = GForm()
    return render_template('signup.html' , form = f  )


@app.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    user = User.query.filter_by(email=email).first() 

    if user: 
        return redirect(url_for('/signup'))

    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('login'))

@app.route('/logout' ,  methods=("GET", "POST"))
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route("/Accueil/")
def Accueil():
    f = GForm()
 
    return render_template("homes.html",title="Other", books=FiltreType1("fire") , form = f  )

@app.route("/supprimer/")
def Supprimer():
    Valeurs = RegistrationForm()
    f = GForm()
    return render_template("supprimer.html",title="Sumpprimer", books=FiltreType1("fire") , form = f )


@app.route("/jeux/" ,  methods=("GET", "POST"))
def jeu():
    f = GForm()
    return render_template("jeux.html", form = f )


@app.route("/compte/" ,  methods=("GET", "POST"))
@login_required
def compteStatus():
    f = GForm()
    if request.method == 'POST' : 
        if request.form.get('MajCompte') != 'None':
            majUser(current_user.id ,request.form.get('email') ,request.form.get('name')) 
    return render_template("Compte.html", books=ListeCompte(current_user.id) , form = f )


@app.route('/supressionCompte' ,  methods=("GET", "POST"))
@login_required
def supCompte():
    SupressionCompte(current_user.id)
    logout_user()   
    return redirect(url_for('home'))

def CurantUser(user) :
    return user 


# Connexion 
@app.route('/login')
def login():
    f = GForm()
    fs = Logins()
    return render_template('login.html' , title = "Connexion" , forms=fs ,form = f)
@app.route('/login', methods=["GET",'POST'])
def login_post():
    print ("other ")
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('login')) # if the user doesn't exist or password is wrong, reload the page

    # if the above check passes, then we know the user has the right credentials
    login_user(user, remember=remember)
    return redirect(url_for('profile'))








@app.route("/ajouter/" , methods=("GET", "POST"))
def ajouter():
    f = GForm()
    fs = ajouters()
    if f.validate_on_submit():
        print(fs.NomPokemon.data)
        AjouterPokemon(str(fs.NomPokemon.data) ,int(fs.generation.data) , int(fs.Classification.data) ,fs.type12.data , fs.type21.data , int(fs.HP.data) , float(fs.height_m.data) , float(fs.weight_kg.data) , int (fs.speed.data) , float(fs.percentage_male.data ), int (fs.is_legendary.data ) )
        return redirect(url_for('Trie')) 

    return render_template("ajouter.html",title = "Ajouter un Pokemon", books=FiltreType1("fire") ,form = f ,ajouterIn=fs )



@app.route("/trier/")
def Trie():
    f = GForm()
    fs = trie()
    return render_template("trier.html",title="Systeme de Flitre",form = f , trie =fs)



@app.route("/trier/" , methods=("GET", "POST") )
def other():
    f = GForm()
    fs = trie()
    print(fs.is_legendary.data)
    if (str(fs.generation.data) == '8') :  # si on choisi toute les generation
        if(str(fs.is_legendary.data) == '2') :
            legendaire = 0 
            return render_template("homes.html",title="resultat" , books= TrieotherAll(fs.typeTrie.data ,fs.typeTrie2.data , legendaire ) ,form = f , trie =fs)

        else : 
            legendaire = 1 
            return render_template("homes.html",title="resultat" , books= TrieotherAll(fs.typeTrie.data  ,fs.typeTrie2.data, legendaire ) ,form = f , trie =fs)
    else : 


        if(str(fs.is_legendary.data) == '2') :
            legendaire = 0 
            return render_template("homes.html",title="resultat" , books= Trieother(fs.typeTrie.data ,fs.typeTrie2.data, fs.generation.data , legendaire ) ,form = f , trie =fs)

        else : 
            legendaire = 1 
            return render_template("homes.html",title="resultat" , books= Trieother(fs.typeTrie.data  ,fs.typeTrie2.data, fs.generation.data , legendaire ) ,form = f , trie =fs)

    














@app.route("/cible/<string:id>")
def success(id):
    f = GForm()
    
    return render_template("recherche.html",title="Recherche Pokemon", books=recherche(id) ,form = f   )
