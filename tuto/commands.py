import click
from .app import app, db
from .models import Author, Book



@app.cli.command()
def syncdb():
    '''
     Création de toutes les tables de la BD
    '''
    db.create_all()




@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    '''
     Create all tables and populate them with data in filename
    '''
    db.create_all()

    import yaml
    books = yaml.load(open(filename),Loader=yaml.FullLoader)
    
    for b in books:
        book = Book(
                    name   = b["name"],
                    generation   = b["generation"],
                    classfication     = b["classfication"],
                    type1   = b["type1"],
                    type2   = b["type2"],
                    hp   = b["hp"],
                    height_m   = b["height_m"],
                    weight_kg   = b["weight_kg"],
                    speed   = b["speed"],
                    percentage_male   = b["percentage_male"],
                    is_legendary   = b["is_legendary"]
                    ) 
        print("ddqdzq")

    # On ajoute l'objet o à la Base :
        db.session.add(book)
    db.session.commit()

#11