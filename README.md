<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/bryan.jeannot974/projetflask">
    <img src="imagesreadme/logo.jpg" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">PokeDex</h3>

  <p align="center">
    Un site de recherche de pokémon fait en Flask/Boostrap
    <br />
    <a href="https://gitlab.com/bryan.jeannot974/projetflask"><strong>Explorer le répertoire</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/bryan.jeannot974/projetflask">Voir la démo</a>
    ·
    <a href="https://gitlab.com/bryan.jeannot974/projetflask/issues">Reporter un bug</a>
    ·
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Sommaire</summary>
  <ol>
    <li>
      <a href="#about-the-project">A propos du projet</a>
      <ul>
        <li><a href="#built-with">Crée avec</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Pour commencer</a>
      <ul>
        <li><a href="#prerequisites">Prérequis</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Remerciements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## A Propos du projet

[![Product Name Screen Shot][product-screenshot]](https://example.com)

Projet en 2ème année de DUT Informatique crée par`bryan.jeannot974, zgabi10, yildizenes101 ` pour notre cours de Web Seveur dirigé par M. Rozavolsgi .

<p align="right">(<a href="#top">Aller en haut</a>)</p>



### Crée avec


* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Flask](https://flask.palletsprojects.com)

<p align="right">(<a href="#top">retour en haut de la page</a>)</p>



<!-- GETTING STARTED -->
## Pour commencer

Pour lancer l'application il faut:

* Se placer dans un dossier vierge git clone https://gitlab.com/bryan.jeannot974/projetflask.git

* Installation

```sh
virtual venv 
source venv/bin/activate 
cd projet-flask-perso/ 
pip install -r requierements.txt
cd tuto/
flask loaddb pokemon_info.yaml 
cd .. 
flask run
```


<p align="right">(<a href="#top">retour en haut de la page</a>)</p>



<!-- USAGE EXAMPLES -->
## Utilisation de notre site

L'utilisation de notre site est simple. Vous arrivez tout d'abord a la page de connexion, ensuite vous aurez accès a la page d'Accueil. Depuis cette page vous pourrez naviguer vers les pages: 
- Ajouter (Vous pourrez ajouter un pokemon aux choix)
- Supprimer (Vous pourrez tout simplement supprimmer un pokemon)
- Trier (Vous pourrez trier vos pokemons, par types, génération, nom... et d'autres)
- Une page jeux 
- Une page Profil pour se connecter, s'inscrire et modifier également ces inforamtions personelles

Le site est très simple d'utilisation mais si vous avez des questions n'hesitez pas a nous contacter, nos coordonnées seront afficher un peu plus bas.

<p align="right">(<a href="#top">retour en haut de la page</a>)</p>



<!-- ROADMAP -->
## Roadmap

### Fonctionalités attendues


- [] Modèle de la BD des pokemons 
- [] Affichage paginé des pokemon, détail au survol ou au click
- [X] Intégration Boostrap4 ou autre lib de CSS
- [] Edition d'un pokemon
- [] Recherche un peu plus avancée de pokemons (par type, generation, legendaire, etc.)
- [X] commande d'import de données (loaddb)
- [X] commande de création des tables (syncdb)


### Fonctionalités souhaitées
- [] Edition/Suppression/Update Albums
- [] Edition/Suppression/Update Artistes
- [] Login (commandes adduser, password)
- [] Playlist par user
- [] Fonctionnalités possibles
- [] Edition/Suppression/Update Genres
- [] Auto-inscription sur le site
- [] Ecouter des extraits (connexion à des APIs)
- [] Noter les albums
- [] Importer des albums d'une API


Voir le [open issues](https://gitlab.com/bryan.jeannot974/projetflask/issues) pour une liste complète des fonctionnalités proposées (et des problèmes connus).

<p align="right">(<a href="#top">retour en haut de la page</a>)</p>



<!-- CONTRIBUTING -->
## Contribution

Les contributions sont ce qui fait de la communauté open source un endroit si incroyable pour apprendre, inspirer et créer. Toutes les contributions que vous faites sont **grandement apprécié**.

Si vous avez une suggestion qui améliorerait cela, veuillez bifurquer le référentiel et créer une pull request.
N’oubliez pas de donner une étoile au projet ! Merci encore!

1. Fork le projet
2. Créer votre branche d’entités 
3. Commit les modifications 
4. Push vers la branche principale 
5. Ouvre un Pull Request

<p align="right">(<a href="#top">retour en haut de la page</a>)</p>





<!-- CONTACT -->
## Contact

Enes YILDIZ - yildizenes101@gmail.com
Bryan JEANNOT - bryan.jeannot974@gmail.com
Gabriel BRIZIOU - gab2.briz@gmail.com
Project Link: [https://gitlab.com/bryan.jeannot974/projetflask](https://gitlab.com/bryan.jeannot974/projetflask)

<p align="right">(<a href="#top">retour en haut de la page</a>)</p>







<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://gitlab.com/bryan.jeannot974/projetflask/-/forks
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://gitlab.com/bryan.jeannot974/projetflask/-/starrers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://gitlab.com/bryan.jeannot974/projetflask/-/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: https://gitlab.com/bryan.jeannot974/projetflask/-/blob/main/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
[product-screenshot]: imagesreadme/screenshot.png
